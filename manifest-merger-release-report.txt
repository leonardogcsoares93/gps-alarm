-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:5:5
	android:name
		ADDED from AndroidManifest.xml:5:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:6:5
	android:name
		ADDED from AndroidManifest.xml:6:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.DISABLE_KEYGUARD
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-feature#0x00020000
ADDED from AndroidManifest.xml:14:5
	android:glEsVersion
		ADDED from AndroidManifest.xml:15:9
	android:required
		ADDED from AndroidManifest.xml:16:9
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
application
ADDED from AndroidManifest.xml:24:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:cardview-v7:21.0.3:16:5
MERGED from com.android.support:recyclerview-v7:21.0.3:17:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.melnykov:floatingactionbutton:1.2.0:12:5
MERGED from com.android.support:recyclerview-v7:21.0.3:17:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:27:9
	android:allowBackup
		ADDED from AndroidManifest.xml:25:9
	android:icon
		ADDED from AndroidManifest.xml:26:9
	android:theme
		ADDED from AndroidManifest.xml:28:9
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:29:9
MERGED from com.google.android.gms:play-services:6.5.87:21:9
	android:value
		ADDED from AndroidManifest.xml:31:13
	android:name
		ADDED from AndroidManifest.xml:30:13
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:32:9
	android:value
		ADDED from AndroidManifest.xml:34:13
	android:name
		ADDED from AndroidManifest.xml:33:13
service#inc.vidistis.leonardogcsoares.gpsalarm.GeofenceTransitionsIntentService
ADDED from AndroidManifest.xml:38:9
	android:name
		ADDED from AndroidManifest.xml:38:18
receiver#inc.vidistis.leonardogcsoares.gpsalarm.MyAdminReceiver
ADDED from AndroidManifest.xml:41:9
	android:permission
		ADDED from AndroidManifest.xml:43:13
	android:name
		ADDED from AndroidManifest.xml:42:13
meta-data#android.app.device_admin
ADDED from AndroidManifest.xml:44:13
	android:resource
		ADDED from AndroidManifest.xml:46:17
	android:name
		ADDED from AndroidManifest.xml:45:17
intent-filter#android.app.action.DEVICE_ADMIN_ENABLED
ADDED from AndroidManifest.xml:48:13
action#android.app.action.DEVICE_ADMIN_ENABLED
ADDED from AndroidManifest.xml:49:17
	android:name
		ADDED from AndroidManifest.xml:49:25
activity#inc.vidistis.leonardogcsoares.gpsalarm.MapsActivity
ADDED from AndroidManifest.xml:53:9
	android:label
		ADDED from AndroidManifest.xml:55:13
	android:launchMode
		ADDED from AndroidManifest.xml:56:13
	android:name
		ADDED from AndroidManifest.xml:54:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:57:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:58:17
	android:name
		ADDED from AndroidManifest.xml:58:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:60:17
	android:name
		ADDED from AndroidManifest.xml:60:27
activity#inc.vidistis.leonardogcsoares.gpsalarm.SearchableActivity
ADDED from AndroidManifest.xml:64:9
	android:launchMode
		ADDED from AndroidManifest.xml:65:13
	android:name
		ADDED from AndroidManifest.xml:64:19
intent-filter#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:66:13
action#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:67:17
	android:name
		ADDED from AndroidManifest.xml:67:25
meta-data#android.app.searchable
ADDED from AndroidManifest.xml:70:13
	android:resource
		ADDED from AndroidManifest.xml:72:17
	android:name
		ADDED from AndroidManifest.xml:71:17
activity#inc.vidistis.leonardogcsoares.gpsalarm.AddAlarmActivity
ADDED from AndroidManifest.xml:75:9
	android:label
		ADDED from AndroidManifest.xml:77:13
	android:theme
		ADDED from AndroidManifest.xml:78:13
	android:name
		ADDED from AndroidManifest.xml:76:13
activity#inc.vidistis.leonardogcsoares.gpsalarm.MyAlarmsActivity
ADDED from AndroidManifest.xml:82:9
	android:label
		ADDED from AndroidManifest.xml:84:13
	android:theme
		ADDED from AndroidManifest.xml:85:13
	android:name
		ADDED from AndroidManifest.xml:83:13
activity#inc.vidistis.leonardogcsoares.gpsalarm.WakeAlarmActivity
ADDED from AndroidManifest.xml:89:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:92:13
	android:launchMode
		ADDED from AndroidManifest.xml:93:13
	android:theme
		ADDED from AndroidManifest.xml:91:13
	android:name
		ADDED from AndroidManifest.xml:90:13
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:cardview-v7:21.0.3:15:5
MERGED from com.android.support:recyclerview-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.melnykov:floatingactionbutton:1.2.0:8:5
MERGED from com.android.support:recyclerview-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
activity#android.support.v7.widget.TestActivity
ADDED from com.android.support:recyclerview-v7:21.0.3:18:9
MERGED from com.android.support:recyclerview-v7:21.0.3:18:9
	android:label
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:19
	android:name
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:60

package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;
import inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase.LocationDbHelper;
import inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase.LocationDbWrapper;
import inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase.MyLocations;

/**
 * Created by leonardogcsoares on 2/26/2015.
 */
public class SearchableActivity extends Activity{

    protected static final String TAG = "SearchableActivity";

    protected InputMethodManager imm;
    private FrameLayout backButton;
    private List<Alarm> mAlarmList;
    private List<MyLocations> mLocationsList;
    private RecyclerView mMyAlarmsRecyclerView;

    private RecyclerView mMyLocationsRecyclerView;
    private SearchView searchView;

    List<Address> mAddresses;

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Log.d(TAG, "onCreate called");

        mActivity = this;

        AdView mAdView = (AdView) findViewById(R.id.search_activity_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.activity_search_searchView_id);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);


        // Guarentees the SearchView is able to collapse on onBackPressed()...
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    searchView.onActionViewCollapsed();
                }
            }
        });

        /** RecyclerView created for locations **/
        mMyLocationsRecyclerView = (RecyclerView) findViewById(R.id.search_activity_locations_recyclerView);
        mMyLocationsRecyclerView.setHasFixedSize(true);

        LocationDbHelper locationDbHelper = new LocationDbHelper(getBaseContext());
        LocationDbWrapper mLocationDbWrapper = new LocationDbWrapper(locationDbHelper);
        mLocationsList = mLocationDbWrapper.getLocations();

        Log.d(TAG, "mLocationList size: " + Integer.toString(mLocationsList.size()));

        final LinearLayoutManager layoutManagerLocations = new LinearLayoutManager(this);
        layoutManagerLocations.setOrientation(LinearLayoutManager.VERTICAL);
        mMyLocationsRecyclerView.setLayoutManager(layoutManagerLocations);
        mMyLocationsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        RecentLocationsAdapter locationsAdapter = new RecentLocationsAdapter();
        mMyLocationsRecyclerView.setAdapter(locationsAdapter);

        FrameLayout.LayoutParams params_locations = (FrameLayout.LayoutParams) mMyLocationsRecyclerView.getLayoutParams();
        params_locations.height = locationsAdapter.getItemCount() * getResources().getDimensionPixelSize(R.dimen.search_locations_row_height);
        mMyLocationsRecyclerView.setLayoutParams(params_locations);

        /** RecyclerView created for alarms**/
        mMyAlarmsRecyclerView = (RecyclerView) findViewById(R.id.search_activity_myAlarms_recyclerView);
//        Improves performance by telling the recycler view that changes in content won't affect the layout size of the view
        mMyAlarmsRecyclerView.setHasFixedSize(true);

        final AlarmsDBWrapper mAlarmsDBWrapper = new AlarmsDBWrapper();
        try {
            mAlarmList = new AsyncTask<Void, Void, List<Alarm>>() {

                @Override
                protected List<Alarm> doInBackground(Void... params) {
                    return mAlarmsDBWrapper.getAlarms();
                }
            }.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        final LinearLayoutManager layoutManagerAlarms = new LinearLayoutManager(this);
        layoutManagerAlarms.setOrientation(LinearLayoutManager.VERTICAL);
        mMyAlarmsRecyclerView.setLayoutManager(layoutManagerAlarms);
        mMyAlarmsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        AlarmAdapter alarmAdapter = new AlarmAdapter();
        mMyAlarmsRecyclerView.setAdapter(alarmAdapter);

        // Auto-adjust the recyclerView size for Alarms.
        FrameLayout.LayoutParams params_alarms = (FrameLayout.LayoutParams) mMyAlarmsRecyclerView.getLayoutParams();
        params_alarms.height = alarmAdapter.getItemCount() * getResources().getDimensionPixelSize(R.dimen.search_myalarm_row_height);
        mMyAlarmsRecyclerView.setLayoutParams(params_alarms);

        //Removes Icon from SearchView hint
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) searchView.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        magImage.setVisibility(View.GONE);


        /** Sets the back button functionality **/
        backButton = (FrameLayout) findViewById(R.id.search_activity_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        /*** Activity receives the search query, and realizes the search ***/
        handleIntent(getIntent());

    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed called");
        super.onBackPressed();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            realizeSearch(query);
            Log.d(TAG, "After realizeSearch is called");
        }
    }

    private void realizeSearch(String query) {
        Log.d(TAG, "realizeSearch is being called");
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());

        mAddresses = null;
        try {

            mAddresses = geocoder.getFromLocationName(query, 1);

            LocationDbWrapper locationDbWrapper = new LocationDbWrapper();
            long rowId = locationDbWrapper.addLocation(new MyLocations(0, query));
            Log.d(TAG, "Row id returned: " + Long.toString(rowId));

            Intent returnIntent = new Intent();
            returnIntent.putExtra("latitude", mAddresses.get(0).getLatitude());
            returnIntent.putExtra("longitude", mAddresses.get(0).getLongitude());
            returnIntent.putExtra("type", "query");
            Log.d(TAG, "Latitude: " + Double.toString(mAddresses.get(0).getLatitude()));
            Log.d(TAG, "Longitude: " + Double.toString(mAddresses.get(0).getLongitude()));
            setResult(RESULT_OK, returnIntent);

        } catch (IOException e) {
            e.printStackTrace();
            setResult(RESULT_CANCELED);
        }

        this.finish();
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");

    }


    public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder> {

        public class AlarmViewHolder extends RecyclerView.ViewHolder {

            private TextView myAlarmTextView;
            private LinearLayout myAlarmLinearLayout;

            public AlarmViewHolder(View view) {
                super(view);

                this.myAlarmTextView = (TextView) view.findViewById(R.id.search_myalarm_row);
                this.myAlarmLinearLayout = (LinearLayout) view.findViewById(R.id.search_myalarm_row_linearLayout);
            }

        }

        @Override
        public AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.search_myalarm_row, parent, false);

            return new AlarmViewHolder(v);

        }


        @Override
        public void onBindViewHolder(AlarmViewHolder holder, final int position) {
            holder.myAlarmTextView.setText(mAlarmList.get(position).getLabel());
            holder.myAlarmLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("latitude", mAlarmList.get(position).getLatitude());
                    returnIntent.putExtra("longitude", mAlarmList.get(position).getLongitude());
                    returnIntent.putExtra("radius", mAlarmList.get(position).getDistance());
                    returnIntent.putExtra("label", mAlarmList.get(position).getLabel());
                    returnIntent.putExtra("type", "alarm");
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
        }
        @Override
        public int getItemCount() {
            return mAlarmList.size();
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

    }

    public class RecentLocationsAdapter extends RecyclerView.Adapter<RecentLocationsAdapter.RecentLocationsViewHolder> {

        public class RecentLocationsViewHolder extends RecyclerView.ViewHolder {

            protected TextView mRecentLocationsTextView;
            protected LinearLayout mRecentLocationsLayout;

            public RecentLocationsViewHolder(View view) {
                super(view);

                this.mRecentLocationsTextView = (TextView) view.findViewById(R.id.recent_locations_myalarm_row_textView);
                this.mRecentLocationsLayout = (LinearLayout) view.findViewById(R.id.recent_locations_myalarm_row_linearLayout);

            }

        }

        @Override
        public RecentLocationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.search_recent_locations_row, parent, false);


            return new RecentLocationsViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final RecentLocationsViewHolder holder, int position) {
            Log.d(TAG, "onBindViewHolder called");
            holder.mRecentLocationsTextView.setText(mLocationsList.get(mLocationsList.size() - 1 - position).getLocation());
            Log.d(TAG, "LocationList is set.");
            holder.mRecentLocationsLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String query = holder.mRecentLocationsTextView.getText().toString();
                    realizeSearch(query);
                }
            });
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public int getItemCount() {
            return mLocationsList.size();
        }
    }


}


package inc.vidistis.leonardogcsoares.gpsalarm.locationGeofence;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.WakeAlarmActivity;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBHelper;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;

/**
 * Created by leonardogcsoares on 11/25/2015.
 */
public class LocationReceiver extends BroadcastReceiver {

    private static final String TAG = "LocationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        Location updateLocation = (Location) intent.getExtras().get(LocationManager.KEY_LOCATION_CHANGED);
//        Log.d(TAG, intent.toString());

        AlarmsDBHelper alarmsDBHelper = new AlarmsDBHelper(context);
        AlarmsDBWrapper alarmsDBWrapper = new AlarmsDBWrapper(alarmsDBHelper);
        List<Alarm> alarms = alarmsDBWrapper.getAlarms();

        Toast.makeText(context, "Updated location!", Toast.LENGTH_SHORT).show();

        try {
            for (Alarm alarm : alarms) {
                if (!alarm.isOn())
                    continue;

                Location alarmLocation = new Location("dummy");
                alarmLocation.setLatitude(alarm.getLatitude());
                alarmLocation.setLongitude(alarm.getLongitude());
                if (updateLocation.distanceTo(alarmLocation) < alarm.getDistance()) {
                    Intent alarmIntent = new Intent(context, WakeAlarmActivity.class);
                    alarmIntent.putExtra("REQUEST_ID", alarm.getLabel());
                    alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(alarmIntent);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}

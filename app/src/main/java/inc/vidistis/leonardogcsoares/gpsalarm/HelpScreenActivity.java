package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.viewpagerindicator.CirclePageIndicator;

import info.hoang8f.widget.FButton;

/**
 * Created by leonardogcsoares on 7/25/2015.
 */
public class HelpScreenActivity extends FragmentActivity {

    private final static String TAG = "HelpScreenActivity";
    private static final int pagerCount = 3;

    private HelpScreenPagerAdapter mHelpScreenPagerAdapter;
    private ViewPager mViewPager;

    private FButton positiveButton;
    private FButton negativeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_screen);

        positiveButton = (FButton) findViewById(R.id.positive_help_fbutton);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Turn off Help on Start
                //Go back to main map
            }
        });
        negativeButton = (FButton) findViewById(R.id.negative_help_fbutton);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Don't turn off Help on start
                //Go back to main map
            }
        });

        mHelpScreenPagerAdapter = new HelpScreenPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.help_screen_viewPager);
        CirclePageIndicator pagerIndicator = (CirclePageIndicator) findViewById(R.id.circle_page_indicator);
        mViewPager.setAdapter(mHelpScreenPagerAdapter);
        pagerIndicator.setViewPager(mViewPager);
        pagerIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == pagerCount - 1) {
                    negativeButton.setVisibility(View.VISIBLE);
                    positiveButton.setVisibility(View.VISIBLE);
                }
                else {
                    negativeButton.setVisibility(View.GONE);
                    positiveButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


    public class HelpScreenPagerAdapter extends FragmentPagerAdapter {

        public HelpScreenPagerAdapter (android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(TAG, "Fragment getItem()");

            Fragment fragment = new HelpFragment(position);
            return fragment;
        }

        @Override
        public int getCount() {
            return pagerCount;
        }
    }

    public class HelpFragment extends Fragment {

        RelativeLayout backgroundImage;
        int position;

        public HelpFragment(int position){
            this.position = position;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_help_screen, null);
            switch(position){
                case 0:
                    ((RelativeLayout) rootView.findViewById(R.id.help_screen_background_relativeLayout))
                            .setBackgroundResource(R.drawable.help_screen_01);
                    break;
                case 1:
                    ((RelativeLayout) rootView.findViewById(R.id.help_screen_background_relativeLayout))
                            .setBackgroundResource(R.drawable.help_screen_02);
                    break;
                case 2:
                    ((RelativeLayout) rootView.findViewById(R.id.help_screen_background_relativeLayout))
                            .setBackgroundResource(R.drawable.help_screen_03);
                    break;

            }
            return rootView;
        }
    }
}

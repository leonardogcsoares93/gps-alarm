package inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase;

import android.location.Location;

/**
 * Created by leonardogcsoares on 4/23/2015.
 */
public class MyLocations {

    private int mId;
    private String mLocation;

    public MyLocations(int id, String location){
        this.mId = id;
        this.mLocation = location;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String mLocation) {
        this.mLocation = mLocation;
    }
}

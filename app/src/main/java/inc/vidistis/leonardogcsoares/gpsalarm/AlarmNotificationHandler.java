package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;

/**
 * Created by leonardogcsoares on 7/17/2015.
 */
public class AlarmNotificationHandler {


    public static final int alarmNotificationID = 101;
    private static final String TAG = "AlarmNotificationHandler";

    private AlarmNotificationHandler() {

    }

    public static int getAlarmsOn(List<Alarm> alarms) {
        int alarmsOn = 0;
        for (Alarm alarm : alarms) {
            if (alarm.isOn()) {
                alarmsOn++;
            }
        }
        return alarmsOn;
    }

    public static void updateNotification (NotificationManager notificationManager, Context context, List<Alarm> alarmsList) {

        int numOfAlarmsOn = getAlarmsOn(alarmsList);

        if (numOfAlarmsOn == 0){
            notificationManager.cancel(alarmNotificationID);
        }

        else {
            Intent intent = new Intent(context, MapsActivity.class);
            PendingIntent notifPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notification =
                    new NotificationCompat.Builder(context)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.paper_airplane_notif))
                            .setSmallIcon(R.drawable.paper_airplane_notif)
                            .setContentTitle(context.getString(R.string.app_name))
                            .setContentText(context.getString(R.string.navigation_drawer_my_alarms_title))
                            .setOngoing(true)
                            .setNumber(numOfAlarmsOn)
                            .setColor(context.getResources().getColor(android.R.color.holo_blue_light))
                            .setLights(android.R.color.holo_blue_bright, 1000, 1000)
                            .setContentIntent(notifPendingIntent).build();
//                                .addAction(R.drawable.ic_action_close, getString(R.string.notification_close_button), notifPendingIntent).build();
            notificationManager.notify(alarmNotificationID, notification);
        }


    }


}

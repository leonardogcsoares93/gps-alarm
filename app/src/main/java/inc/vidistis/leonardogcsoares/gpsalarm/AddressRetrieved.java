package inc.vidistis.leonardogcsoares.gpsalarm;

import android.location.Address;

import java.util.List;

/**
 * Created by leonardogcsoares on 6/24/2015.
 */
public interface AddressRetrieved {

    public void onAddressRetrieved(List<Address> addresses);
}

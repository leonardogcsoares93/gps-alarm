package inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase;

import android.provider.BaseColumns;

/**
 * Created by leonardogcsoares on 4/23/2015.
 */
public class LocationEntries implements BaseColumns{
    public static final String TABLE_NAME = "Locations";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_NAME_NULLABLE = "null";
}

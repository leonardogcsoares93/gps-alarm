package inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by leonardogcsoares on 4/23/2015.
 */
public class LocationDbHelper extends SQLiteOpenHelper {

    private static final String COMMA_SEP = ",";
    private static final String VARCHAR_TYPE = " VARCHAR";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + LocationEntries.TABLE_NAME + " (" +
                    LocationEntries.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                    LocationEntries.COLUMN_LOCATION + VARCHAR_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + LocationEntries.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Locations.db";

    public LocationDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDownGrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}

package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melnykov.fab.FloatingActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBHelper;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;
import inc.vidistis.leonardogcsoares.gpsalarm.locationGeofence.LocationReceiver;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        AddressRetrieved {

    private final static int SEARCH_REQUEST_CODE = 1;          // on search button pressed
    private final static int GPS_OFF_REQUEST_CODE = 2;         // on use location positive button pressed
    public final static int MY_ALARMS_REQUEST_CODE = 3;
    public final static int ADD_ALARM_REQUEST_CODE = 4;
    private final int ADMIN_INTENT_REQUEST_CODE = 15;   // for unlock screen capabilities

    protected static final String TAG = "MapsActivity";

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    protected static GoogleApiClient mGoogleApiClient;
    private LocationManager mLocationManager;

    private boolean onActivityResultTest = false;

    // Created so as to never have another marker on the map.
    Marker newMarker = null;
    Circle newMarkerCircle = null;
    String mCurrentLocationName;
    private AlarmsDBWrapper alarmsDBWrapper;


    private DrawerLayout mDrawerLayout;
    private ListView mDrawerListView;
    private String[] navigationDrawerTitles;
    private TypedArray navigationDrawerIcons;

    private ArrayList<NavigationDrawerItem> navigationDrawerItems;
    private NavigationDrawerListAdapter navigationDrawerListAdapter;

    FloatingActionButton fabAddAlarm;
    FloatingActionButton fabGetLocation;
    CardView new_marker_information_box;
    TextView new_marker_box_address;

    private Context mContext;


    private enum DrawerItem {
        MY_ALARMS(0),
        PLAN_A_TRIP(1),
        SETTINGS(2),
        HELP(3);

        private final int value;

        DrawerItem(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    //Geofencing Variables
//    List<Geofence> mGeofenceList;
    List<Address> mAddresses;

    boolean backPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreateCalled");
        setContentView(R.layout.activity_maps);

        mContext = this;

        AdView mAdView = (AdView) findViewById(R.id.maps_activity_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Every 5 seconds this thread resets the backButton checking to false
        new ResetBackPressedThread().start();

        // This is initially called here so as to make sure that if its the first time the user is opening the app
        // the databases can be created sucessfully.
        AlarmsDBHelper alarmsDBHelper = new AlarmsDBHelper(this);
        alarmsDBWrapper = new AlarmsDBWrapper(alarmsDBHelper);

//        LocationDbHelper locationDbHelper = new LocationDbHelper(this);
//        LocationDbWrapper locationDbWrapper = new LocationDbWrapper(locationDbHelper);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        // Acquires access to the map.
        mMap = mapFragment.getMap();
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(final LatLng latLng) {
                new GetAddressAsync(MapsActivity.this).execute(latLng);
                // Guarentee's the exchange of address
                hideMarkerBox();
                setMarkerPosition(latLng);

            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

//                // Markers that are currently in my alarms continue active and are not removed from map
                if (newMarker != null) {
                    newMarker.remove();
                    newMarker = null;
                    hideMarkerBox();
                }

                if (newMarkerCircle != null) {
                    newMarkerCircle.remove();
                    newMarkerCircle = null;
                }

            }
        });

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

                Location myLocation = mMap.getMyLocation();
                try {
                    new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                    if (!anyLocationProviderOn())
                        buildGpsOffAlertDialog(getString(R.string.dialog_gps_off_title), getString(R.string.dialog_gps_off_message));
                } catch (NullPointerException e) {
                    //Phone is not connected to the internet, unable to retrieve current location
                    //Gps not on, send dialog message with capability to turn GPS on
                    if (!anyLocationProviderOn())
                        buildGpsOffAlertDialog(getString(R.string.dialog_gps_off_title), getString(R.string.dialog_gps_off_message));
                    else {
                        Log.d(TAG, "Send notification telling user to wait");
                        Toast.makeText(mContext, getString(R.string.maps_activity_wait_for_gps), Toast.LENGTH_LONG).show();
                    }

                }

                return false;
            }
        });
        if (mMap.getMyLocation() == null && !bothLocationProvidersOn()) {
            buildGpsOffAlertDialog(getString(R.string.dialog_gps_off_title), getString(R.string.dialog_gps_off_message));
        }
        // Positions the MyLocation button accordingly
        mapFragment.getMap().setPadding(0, HelpFunctions.dpToPx(100, mContext), 0, 0);


        new_marker_information_box = (CardView) findViewById(R.id.new_marker_information_box_id);
        new_marker_box_address = (TextView) findViewById(R.id.new_marker_information_box_description_id);

        fabAddAlarm = (FloatingActionButton) findViewById(R.id.fab_add_alarm);
        fabAddAlarm.hide();
        fabAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClickAddAlarm");
                LatLng latLng = newMarker.getPosition();
                Intent intent = new Intent(mContext, AddAlarmActivity.class);
                intent.putExtra("Location", mCurrentLocationName);
                intent.putExtra("Latitude", latLng.latitude);
                intent.putExtra("Longitude", latLng.longitude);

                hideMarkerBox();
                startActivityForResult(intent, ADD_ALARM_REQUEST_CODE);
            }
        });


        // From array.xml retrieve the list of Buttons (Icon + Title) in the correct order to put in the drawer.
        navigationDrawerTitles = getResources().getStringArray(R.array.navigation_drawer_titles);
        navigationDrawerIcons = getResources().obtainTypedArray(R.array.navigation_drawer_icons);

        mDrawerListView = (ListView) findViewById(R.id.activity_maps_navigation_drawer);

        navigationDrawerItems = new ArrayList<NavigationDrawerItem>();

        for (int i = 0; i < navigationDrawerTitles.length; i++) {
            navigationDrawerItems.add(new NavigationDrawerItem(navigationDrawerTitles[i], navigationDrawerIcons.getResourceId(i, -1)));
        }


        // Keeps code clean for garbage collector. (I assume)
        navigationDrawerIcons.recycle();

        navigationDrawerListAdapter = new NavigationDrawerListAdapter(getApplicationContext(), navigationDrawerItems);
        mDrawerListView.setAdapter(navigationDrawerListAdapter);

        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == DrawerItem.MY_ALARMS.getValue()) {
                    Intent intent = new Intent(mContext, MyAlarmsActivity.class);
                    startActivityForResult(intent, MY_ALARMS_REQUEST_CODE);
                    mDrawerLayout.closeDrawer(Gravity.START);
                }

                if (position == DrawerItem.PLAN_A_TRIP.getValue()) {
                    Intent intent = new Intent(mContext, PlanATripActivity.class);
                    startActivity(intent);
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else if (position == DrawerItem.SETTINGS.getValue()) {

                } else if (position == DrawerItem.HELP.getValue()) {
                    Intent intent = new Intent(mContext, HelpScreenActivity.class);
                    startActivity(intent);
                    mDrawerLayout.closeDrawer(Gravity.START);

                }
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.map_activity_navigation_drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);


        Button searchButton = (Button) findViewById(R.id.maps_activity_search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, SearchableActivity.class);
                startActivityForResult(intent, SEARCH_REQUEST_CODE);
            }
        });

        ImageView navigationDrawerButton = (ImageView) findViewById(R.id.activity_maps_navigation_drawer_imageButton);
        navigationDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.START);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onActivityResultTest = true;
        mMap.getMyLocation();
        if (requestCode == SEARCH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "SEARCH_REQUEST_CODE");
                LatLng latLng = new LatLng(data.getDoubleExtra("latitude", 0), data.getDoubleExtra("longitude", 0));

                // Query type
                if (data.getStringExtra("type").equals("query")) {
                    new GetAddressAsync(MapsActivity.this).execute(latLng);
                }

//                 Moves the camera to a position returned
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(17)
                        .bearing(0)
                        .tilt(0)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                // Alarm clicked to be viewed
                if (data.getStringExtra("type").equals("alarm")) {
                    setAlarmMarkerPosition(latLng, data.getIntExtra("radius", 1000), data.getStringExtra("label"));
                } else
                    setMarkerPosition(latLng);
            }
        } else if (requestCode == ADMIN_INTENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Log.d(TAG, "Registered as admin...");
            } else
                Log.d(TAG, "Not Registered as admin...");
        } else if (requestCode == MY_ALARMS_REQUEST_CODE) {
            mGoogleApiClient.reconnect();
            if (data != null) {
                LatLng latLng = new LatLng(data.getDoubleExtra("latitude", 0), data.getDoubleExtra("longitude", 0));
//                 Moves the camera to a position returned
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)
                        .zoom(14)
                        .bearing(0)
                        .tilt(0)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                setMarkerPosition(latLng);

                // Alarm clicked to be viewed
                setAlarmMarkerPosition(latLng, data.getIntExtra("radius", 1000), data.getStringExtra("label"));
            }
        } else if (requestCode == ADD_ALARM_REQUEST_CODE) {
            mGoogleApiClient.reconnect();
            LatLng latLng = new LatLng(data.getDoubleExtra("latitude", 0), data.getDoubleExtra("longitude", 0));
//                 Moves the camera to a position returned
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)
                    .zoom(14)
                    .bearing(0)
                    .tilt(0)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            setAlarmMarkerPosition(latLng, data.getIntExtra("radius", 1000), data.getStringExtra("label"));
        }
    }

    @Override
    public void onBackPressed() {

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (backPressedOnce) {
            super.onBackPressed();
        }

        if (checkIfAlarmOn()) {
            if (!anyLocationProviderOn()) {
                // Send dialog
                buildGpsOffAlertDialog(getString(R.string.dialog_gps_off_title), getString(R.string.dialog_gps_off_message));
                Log.d(TAG, "Both location providers disabled");
                Log.d(TAG, "GPS Provider status: " + Boolean.toString(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)));
                Log.d(TAG, "Location Network Provider status: " + Boolean.toString(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)));

            } else if (anyLocationProviderOn() && !bothLocationProvidersOn()) {
                buildGpsWrongAlertDialog(getString(R.string.dialog_gps_only_title), getString(R.string.dialog_gps_only_message));
                Log.d(TAG, "GPS Provider status: " + Boolean.toString(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)));
                Log.d(TAG, "Location Network Provider status: " + Boolean.toString(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)));
            } else if (bothLocationProvidersOn()) {
                // Tell user to wait
                if (mMap.getMyLocation() == null) {
                    Toast.makeText(this, getString(R.string.toast_backPress_warning), Toast.LENGTH_LONG).show();
                    Log.d(TAG, "GPS Provider status: " + Boolean.toString(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)));
                    Log.d(TAG, "Location Network Provider status: " + Boolean.toString(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)));
                }
            }

        }
        synchronized (this) {
            backPressedOnce = true;
        }

    }

    private class ResetBackPressedThread extends Thread {
        public synchronized void run() {
            try {
                wait(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            backPressedOnce = false;
        }
    }


    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        buildGoogleApiClient();
        super.onResume();
    }

    @Override
    protected void onPause() {

        if (isFinishing()) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }


        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private void showMarkerBox(String address) {

        if (!fabAddAlarm.isVisible()) {
            fabAddAlarm.show();
            new_marker_box_address.setText(address);
            new_marker_information_box.setVisibility(View.VISIBLE);
        }
    }

    private void hideMarkerBox() {
        if (fabAddAlarm.isVisible()) {
            fabAddAlarm.hide();
            new_marker_information_box.setVisibility(View.GONE);
        }

    }

    private void setMarkerPosition(LatLng latLng) {

        if (newMarker != null) {
            newMarker.remove();
            newMarker = null;
        }

        if (newMarkerCircle != null) {
            newMarkerCircle.remove();
            newMarkerCircle = null;
        }

        newMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng));

    }

    private void setAlarmMarkerPosition(LatLng latLng, int radius, String label) {
        if (newMarker != null) {
            newMarker.remove();
            newMarker = null;
        }

        if (newMarkerCircle != null) {
            newMarkerCircle.remove();
            newMarkerCircle = null;
        }

        newMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(label)
                .snippet(getString(R.string.alarm_marker_snippet_intro) + Integer.toString(radius) + getString(R.string.alarm_marker_meters)));
        newMarker.showInfoWindow();

        CircleOptions circleOptions = new CircleOptions()
                .strokeColor(Color.BLACK)
                .strokeWidth(0)
                .fillColor(Color.parseColor("#7D2196F3"))
                .center(latLng)
                .radius(radius);
        newMarkerCircle = mMap.addCircle(circleOptions);

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                newMarker.remove();
                newMarkerCircle.remove();
                newMarker = null;
                newMarkerCircle = null;
                Intent intent = new Intent(mContext, MyAlarmsActivity.class);
                startActivityForResult(intent, MY_ALARMS_REQUEST_CODE);
            }
        });


    }


    private class GetAddressAsync extends AsyncTask<LatLng, Void, List<Address>> {
        private AddressRetrieved mCallback;

        public GetAddressAsync(Context context) {
            this.mCallback = (AddressRetrieved) context;
        }

        @Override
        protected List<Address> doInBackground(LatLng... params) {


            LatLng latLng = params[0];
            List<Address> addresses = null;
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                // Position for the last marker

            } catch (IOException e) {
                e.printStackTrace();
            }

            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            mCallback.onAddressRetrieved(addresses);
        }
    }

    @Override
    public void onAddressRetrieved(List<Address> addresses) {
        mAddresses = addresses;
        Log.d(TAG, "onAddressRetrieved");
        if (mAddresses != null && mAddresses.size() != 0) {
            mCurrentLocationName = mAddresses.get(0).getAddressLine(0);
            showMarkerBox(mCurrentLocationName);
        } else {
            //In case of error or no internet connection
            mCurrentLocationName = getResources().getString(R.string.address_not_found);
            showMarkerBox(mCurrentLocationName);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {


        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null && !onActivityResultTest) {

            // Moves the camera to a position that shows the current location of the user.
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                    .zoom(17)
                    .bearing(0)
                    .tilt(0)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }

        Intent intent = new Intent(this, LocationReceiver.class);
        PendingIntent locationIntent = PendingIntent.getBroadcast(getApplicationContext(), 1000, intent, PendingIntent.FLAG_CANCEL_CURRENT);



        if (bothLocationProvidersOn()) {
            mLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 30000, 500, locationIntent);
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 500, locationIntent);
        }
//
//        else {
//            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30, 500, locationIntent);
//        }
//        if (mGoogleApiClient.isConnected())
//            new GeofencingHandler.AddGeofencesAsync(mContext, mGoogleApiClient, alarmsDBWrapper.getAlarms()).execute();
//        else
//            mGoogleApiClient.reconnect();

    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.reconnect();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
        Log.d(TAG, "Wifi or 3g not working, Geofences not registered.");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }


    /**
     * True if location provider ON, False if OFF
     **/
    private boolean anyLocationProviderOn() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
//                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    private boolean bothLocationProvidersOn() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
//                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    // Can be changed later on to a Builder Pattern
    public void buildGpsOffAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(message)
                .setTitle(title)
                .setPositiveButton(R.string.dialog_gps_off_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(gpsSettingIntent, GPS_OFF_REQUEST_CODE);
                    }
                })
                .setNegativeButton(R.string.dialog_gps_off_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.show();
    }

    public void buildGpsWrongAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(message)
                .setTitle(title)
                .setPositiveButton(R.string.dialog_gps_only_Ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent gpsSettingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(gpsSettingIntent, GPS_OFF_REQUEST_CODE);
                    }
                })
                .setNegativeButton(R.string.dialog_gps_only_Cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "negative button clicked");
                    }
                });

        builder.show();
    }

    /**
     * Checks if any alarm is on, True = Yes, False = No
     **/
    private boolean checkIfAlarmOn() {

        List<Alarm> alarmsList = alarmsDBWrapper.getAlarms();
        for (int i = 0; i < alarmsList.size(); i++) {
            if (alarmsList.get(i).isOn())
                return true;
        }

        return false;
    }

}

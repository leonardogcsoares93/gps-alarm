package inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonardogcsoares on 3/24/2015.
 */
public class AlarmsDBWrapper{

    private static SQLiteOpenHelper mAlarmsDbHelper;
    private static final String TAG = "AlarmsDBWrapper";

    public AlarmsDBWrapper(SQLiteOpenHelper alarmsDbHelper) {
        this.mAlarmsDbHelper = alarmsDbHelper;
    }

    public AlarmsDBWrapper() {

    }

    public int getAlarmsCount(){

        SQLiteDatabase db = mAlarmsDbHelper.getReadableDatabase();

        String[] projection = {
                AlarmEntries.COLUMN_ALARM_ID
        };

        Cursor cursor = db.query(
                AlarmEntries.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        if(cursor != null) {
            int returnValue = cursor.getCount();
            cursor.close();
            return returnValue;
        }
        else
            return 0;

    }

    // Returns the rowId number for the new row inserted
    public long addAlarm(Alarm alarm) {

        SQLiteDatabase db = mAlarmsDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
//        values.put(AlarmEntries.COLUMN_ALARM_ID, alarm.getId());
        values.put(AlarmEntries.COLUMN_ALARM_LATITUDE, alarm.getLatitude());
        values.put(AlarmEntries.COLUMN_ALARM_LONGITUDE, alarm.getLongitude());
        values.put(AlarmEntries.COLUMN_ALARM_LOCATION, alarm.getLocation());
        values.put(AlarmEntries.COLUMN_ALARM_DISTANCE, alarm.getDistance());
        values.put(AlarmEntries.COLUMN_ALARM_LABEL, alarm.getLabel());
        values.put(AlarmEntries.COLUMN_ALARM_VIBRATE, alarm.isVibrate());
        values.put(AlarmEntries.COLUMN_ALARM_WHATSAPP, alarm.isWhatsapp());
        values.put(AlarmEntries.COLUMN_ALARM_PEOPLE, alarm.getPeople());
        values.put(AlarmEntries.COLUMN_ALARM_NUMBERS, alarm.getNumbers());
        values.put(AlarmEntries.COLUMN_ALARM_CUSTOM_MESSAGE, alarm.getMessage());
        values.put(AlarmEntries.COLUMN_ALARM_IS_ON, true);

        long longRowId;
        longRowId = db.insert(AlarmEntries.TABLE_NAME,
                AlarmEntries.COLUMN_NAME_NULLABLE,
                values);

        return longRowId;
    }

    // Returns the number of rows deleted, in this case if the alarm was successfully deleted.
    public int removeAlarm(Alarm alarm, int size){

        SQLiteDatabase db = mAlarmsDbHelper.getWritableDatabase();

//        Log.d(TAG, "Table name: " + AlarmEntries.TABLE_NAME);
//        Log.d(TAG, "Column Alarm ID: " + AlarmEntries.COLUMN_ALARM_ID);
//        Log.d(TAG, "Alarm ID: " + Integer.toString(alarm.getId()));

        int row;

        if (size == 1)
            row = db.delete(AlarmEntries.TABLE_NAME, null, null);
        else
            row = db.delete(
                    AlarmEntries.TABLE_NAME,
                    AlarmEntries.COLUMN_ALARM_ID + "=" + Integer.toString(alarm.getId()),
                    null);

//        Log.d(TAG, "Row deleted: " + Integer.toString(row));

        return row;

    }

    public List<Alarm> getAlarms(){

        SQLiteDatabase db = mAlarmsDbHelper.getReadableDatabase();

        Cursor cursor = db.query(AlarmEntries.TABLE_NAME,
                new String[] { AlarmEntries.COLUMN_ALARM_ID,
                        AlarmEntries.COLUMN_ALARM_LATITUDE,
                        AlarmEntries.COLUMN_ALARM_LONGITUDE,
                        AlarmEntries.COLUMN_ALARM_LOCATION,
                        AlarmEntries.COLUMN_ALARM_DISTANCE,
                        AlarmEntries.COLUMN_ALARM_LABEL,
                        AlarmEntries.COLUMN_ALARM_VIBRATE,
                        AlarmEntries.COLUMN_ALARM_WHATSAPP,
                        AlarmEntries.COLUMN_ALARM_PEOPLE,
                        AlarmEntries.COLUMN_ALARM_NUMBERS,
                        AlarmEntries.COLUMN_ALARM_CUSTOM_MESSAGE,
                        AlarmEntries.COLUMN_ALARM_IS_ON
                },
                null, null, null, null, null);



        List<Alarm> alarms = new ArrayList<>();
        Alarm alarm;

        int mId;
        double mLatitude;
        double mLongitude;
        String mLocation;
        int mDistance;
        String mLabel;
        boolean mVibrate;
        boolean mWhatsapp;
        String mPeople;
        String mNumbers;
        String mMessage;
        boolean mIsOn;

        System.out.println(Boolean.toString(cursor.moveToFirst()));

        int columnIndex = 0;
        for(int row = 0; row < cursor.getCount(); row++) {

            mId = cursor.getInt(columnIndex);
            columnIndex++;

            mLatitude = cursor.getDouble(columnIndex);
            columnIndex++;

            mLongitude = cursor.getDouble(columnIndex);
            columnIndex++;

            mLocation = cursor.getString(columnIndex);
            columnIndex++;

            mDistance = cursor.getInt(columnIndex);
            columnIndex++;

            mLabel = cursor.getString(columnIndex);
            columnIndex++;

            mVibrate = (cursor.getInt(columnIndex) != 0);
            columnIndex++;

            mWhatsapp = (cursor.getInt(columnIndex) != 0);
            columnIndex++;

            mPeople = cursor.getString(columnIndex);
            columnIndex++;

            mNumbers = cursor.getString(columnIndex);
            columnIndex++;

            mMessage = cursor.getString(columnIndex);
            columnIndex++;

            mIsOn = (cursor.getInt(columnIndex) != 0);
            columnIndex = 0;

            alarm = new Alarm(mId, mLatitude, mLongitude, mLocation, mDistance, mLabel, mVibrate, mWhatsapp, mPeople, mNumbers, mMessage, mIsOn);
            alarms.add(alarm);

            if(row < cursor.getCount()-1)
                cursor.moveToNext();

        }

        cursor.close();

        return alarms;

    }

    public void updateAlarmOnOff(Alarm alarm){

        Log.d(TAG, "updateAlarmOnOff called");
        Log.d(TAG, "Alarm to be updated is on: " + Boolean.toString(alarm.isOn()));

        SQLiteDatabase db = mAlarmsDbHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(AlarmEntries.COLUMN_ALARM_IS_ON, alarm.isOn());

        int rowsChanged = db.update(
                AlarmEntries.TABLE_NAME,
                cv, //values to be updated
                AlarmEntries.COLUMN_ALARM_ID + "=" + Integer.toString(alarm.getId()),
                null
            );

    }

    public void printAlarmValues() {
        SQLiteDatabase db = mAlarmsDbHelper.getWritableDatabase();

        List<Alarm> alarms = getAlarms();

        for (int i=0; i< alarms.size(); i++) {
            Log.d(TAG,alarms.get(i).toString());
        }
    }

}

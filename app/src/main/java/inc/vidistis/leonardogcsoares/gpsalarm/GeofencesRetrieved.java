package inc.vidistis.leonardogcsoares.gpsalarm;


import com.google.android.gms.location.Geofence;

import java.util.List;

/**
 * Created by leonardogcsoares on 6/29/2015.
 */
public interface GeofencesRetrieved {

    public void onGeofencesRetrieved(List<Geofence> geofences);
}

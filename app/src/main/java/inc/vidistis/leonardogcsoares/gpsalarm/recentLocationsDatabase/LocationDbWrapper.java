package inc.vidistis.leonardogcsoares.gpsalarm.recentLocationsDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by leonardogcsoares on 4/23/2015.
 */
public class LocationDbWrapper {

    private static SQLiteOpenHelper mLocationDbHelper;
    private static final String TAG = "LocationDbHelper";

    public LocationDbWrapper(SQLiteOpenHelper locationDbHelper) {
        this.mLocationDbHelper = locationDbHelper;
    }

    public LocationDbWrapper() {

    }

    public int getLocationsCount(){

        SQLiteDatabase db = mLocationDbHelper.getReadableDatabase();

        String[] projection = {
                LocationEntries.COLUMN_ID
        };

        Cursor cursor = db.query(
                LocationEntries.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        if(cursor != null) {
            int returnValue = cursor.getCount();
            cursor.close();
            return returnValue;
        }
        else
            return 0;

    }

    // Returns the rowId number for the new row inserted
    // If returned -1, location already exists, and is not added to the list
    public long addLocation(MyLocations location) {

        List<MyLocations> mLocations = getLocations();


        SQLiteDatabase db = mLocationDbHelper.getWritableDatabase();

        for (MyLocations loc1 : mLocations) {
            if (loc1.getLocation().toLowerCase().equals(location.getLocation().toLowerCase())) {
                Log.d(TAG, "Locations are equal");
                return -1;
            }
        }

        if (mLocations.size() > 3) {
            removeLocation(mLocations.get(0), mLocations.size());
        }


        Log.d(TAG, "Location to add: " + location.getLocation());
        ContentValues values = new ContentValues();
        values.put(LocationEntries.COLUMN_LOCATION, location.getLocation());

        long longRowId;
        longRowId = db.insert(LocationEntries.TABLE_NAME,
                LocationEntries.COLUMN_NAME_NULLABLE,
                values);

        return longRowId;
    }

    // Returns the number of rows deleted, in this case if the alarm was successfully deleted.
    public int removeLocation(MyLocations location, int size){

        SQLiteDatabase db = mLocationDbHelper.getWritableDatabase();

        int row;

        if (size == 1)
            row = db.delete(LocationEntries.TABLE_NAME, null, null);
        else
            row = db.delete(LocationEntries.TABLE_NAME, LocationEntries.COLUMN_ID + "=" + Integer.toString(location.getId()), null);

//        Log.d(TAG, "Row deleted: " + Integer.toString(row));

        return row;

    }

    public List<MyLocations> getLocations(){

        SQLiteDatabase db = mLocationDbHelper.getReadableDatabase();

        Cursor cursor = db.query(LocationEntries.TABLE_NAME,
                new String[] { LocationEntries.COLUMN_ID,
                               LocationEntries.COLUMN_LOCATION},
                null, null, null, null, null);



        List<MyLocations> locations = new ArrayList<>();
        MyLocations location;

        int mId;
        String mLocation;

        System.out.println(Boolean.toString(cursor.moveToFirst()));

        int columnIndex = 0;
        for(int row = 0; row < cursor.getCount(); row++) {

            mId = cursor.getInt(columnIndex);
            columnIndex++;

            mLocation = cursor.getString(columnIndex);
            columnIndex = 0;

            location = new MyLocations(mId, mLocation);
            locations.add(location);

            if(row < cursor.getCount()-1)
                cursor.moveToNext();

        }

        cursor.close();

        return locations;

    }
}

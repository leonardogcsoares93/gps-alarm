package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;


public class AddAlarmActivity extends ActionBarActivity {

    private static String TAG = "AddAlarmActivity";

//    private AlarmsDBHelper mAlarmDbHelper;

    private MultiAutoCompleteTextView mMultiAutoPeopleTextView;
    private Switch vibrateSwitch;
    private Switch whatsappSwitch;
    private ImageView messageImageView;
    private TextView mLocationTextView;
    Vibrator mVibrator;
    private static Cursor mCursor;

    // Data from Activity to Database
    Intent retrievedIntent;
    String mLocationName;
    double mLatitude;
    double mLongitude;
    int mDistance;
    String mLabel;
    boolean mVibrate;
    boolean mWhatsApp;
    List<String> mPeople = null;
    List<String> mNumbers = null;
    String mMessage;

    private Context mContext;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alarm);

        mContext = this;
        // Get Location Information from MapsActivity
        retrievedIntent = getIntent();

        Bundle extras = retrievedIntent.getExtras();
        mLocationName = extras.getString("Location");
        mLatitude = extras.getDouble("Latitude");
        mLongitude = extras.getDouble("Longitude");

        mLocationTextView = (TextView) findViewById(R.id.add_alarm_location_textView);
        mLocationTextView.setText(mLocationName);


        //Initialize the contacts lists to empty.
        mPeople = new ArrayList<String>();
        mNumbers = new ArrayList<String>();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle(getResources().getString(R.string.add_alarm_activity_actionBar_logo));


        final ContentResolver contentResolver = getContentResolver();

        mVibrator = (Vibrator) this.mContext.getSystemService(Context.VIBRATOR_SERVICE);


        // ********** Distance Spinner **********
        Spinner spinner = (Spinner) findViewById(R.id.add_alarm_distance_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.d(TAG, "Spinner Value: " + item);
                String item = (String) parent.getItemAtPosition(position);

                mDistance = Integer.parseInt(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        vibrateSwitch = (Switch) findViewById(R.id.add_alarm_vibrate_switch);
        whatsappSwitch = (Switch) findViewById(R.id.add_alarm_whatapp_switch);
        messageImageView = (ImageView) findViewById(R.id.add_alarm_message_imageView);

        vibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mVibrate = isChecked;

                if (isChecked)
                    mVibrator.vibrate(350);
            }
        });


        whatsappSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mMultiAutoPeopleTextView.setText("");

                if (isChecked) {
                    messageImageView.setImageDrawable(getDrawable(R.drawable.ic_form_whatsapp));

                    // **** Fix because whatsapp search not working
//                    mCursor = contentResolver.query(
//                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                                    PEOPLE_PROJECTION,
//                                    PEOPLE_SELECTION,
//                                    PEOPLE_SELECTION_ARGS, null);

                }
                else {
                    messageImageView.setImageDrawable(getDrawable(R.drawable.ic_form_message));

                }
            }
        });


        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.add_alarm_activity_locations, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        // ***** Add People MultiAutoCompleteTextView *****
        mMultiAutoPeopleTextView = (MultiAutoCompleteTextView) findViewById(R.id.add_alarm_people_autoCompleteText);

        mCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PEOPLE_PROJECTION, null, null, null);
        final ContactListAdapter contactListAdapter = new ContactListAdapter(this, mCursor);

        mMultiAutoPeopleTextView.setAdapter(contactListAdapter);
        mMultiAutoPeopleTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        // Used to get the information of the contact selected
        mMultiAutoPeopleTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor1 = (Cursor) parent.getItemAtPosition(position);
                String peopleItem = cursor1.getString(2);
                String numberItem = cursor1.getString(1);

                mPeople.add(peopleItem);
                mNumbers.add(numberItem);

                Log.d(TAG, cursor1.getString(2) + ", " + cursor1.getString(1));
            }
        });


        // Autohide the soft input keyboard.
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static final String[] PEOPLE_PROJECTION = new String[] {
            ContactsContract.Contacts._ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.Data.MIMETYPE,
    };

    // Not used yet, for future WhatsApp contacts find.
//    private static final String PEOPLE_SELECTION = ContactsContract.Data.MIMETYPE + " =? and account_type=?";
//    private static final String[] PEOPLE_SELECTION_ARGS = {
//            "vnd.android.cursor.item/vnd.com.whatsapp.profile",
//            "com.whatsapp"
//        };

    public static class ContactListAdapter extends CursorAdapter implements Filterable {

        ContentResolver mContent;


        public ContactListAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            mContent = context.getContentResolver();
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            final LayoutInflater inflater = LayoutInflater.from(context);
            final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.add_alarm_people_contacts_listview, parent, false);

            TextView namePeopleTextView = (TextView) linearLayout.findViewById(R.id.add_alarm_people_name_listview);
            TextView numberPeopleTextView = (TextView) linearLayout.findViewById(R.id.add_alarm_people_number_listview);

            namePeopleTextView.setText(cursor.getString(2));
            numberPeopleTextView.setText(cursor.getString(1));

            return linearLayout;

        }


        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            TextView namePeopleTextView = (TextView) view.findViewById(R.id.add_alarm_people_name_listview);
            TextView numberPeopleTextView = (TextView) view.findViewById(R.id.add_alarm_people_number_listview);

            namePeopleTextView.setText(cursor.getString(2));
            numberPeopleTextView.setText(cursor.getString(1));

        }

        @Override
        public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
            if (getFilterQueryProvider() != null) {
                return getFilterQueryProvider().runQuery(constraint);
            }

            StringBuilder buffer = null;
            String[] args = null;
            if (constraint != null) {
                buffer = new StringBuilder();
                buffer.append("UPPER(");
                buffer.append(ContactsContract.Contacts.DISPLAY_NAME);
                buffer.append(") GLOB ?");
                args = new String[] { constraint.toString().toUpperCase() + "*" };
            }

            return mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PEOPLE_PROJECTION,
                    buffer == null ? null : buffer.toString(), args,
                    null);

        }

        @Override
        public CharSequence convertToString(Cursor cursor) {
            return cursor.getString(2) + " (" + cursor.getString(1) + ")";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_alarm_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menu_add_alarm_save:
                saveAlarm();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed");
        // Closes activity and goes back to home screen (MapsActivity)
        this.finish();
    }

    private boolean saveAlarm() {

        AlarmsDBWrapper mAlarmsDBWrapper = new AlarmsDBWrapper();

        EditText labelEditText = (EditText) findViewById(R.id.add_alarm_label_editText);
        mLabel = labelEditText.getText().toString();

        mWhatsApp = whatsappSwitch.isChecked();
        mVibrate = vibrateSwitch.isChecked();

        EditText messageEditText = (EditText) findViewById(R.id.add_alarm_message_editText);
        mMessage = messageEditText.getText().toString();

        //Conditions for Alarm to Save successfully
        if(mLabel.isEmpty())
            mLabel = getString(R.string.add_alarm_alarm_name) + " " + mAlarmsDBWrapper.getAlarmsCount();

        Log.d(TAG, Integer.toString(mAlarmsDBWrapper.getAlarmsCount()));

        if (!mPeople.isEmpty() && mMessage.isEmpty()) {
            messageEditText.requestFocus();
            Toast.makeText(mContext, getString(R.string.add_alarm_message_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        if (mLocationName == null)
            mLocationName = "Near some location...";

        Alarm alarm = new Alarm(mLatitude, mLongitude, mLocationName, mDistance, mLabel, mVibrate, mWhatsApp, mPeople.toString(), mNumbers.toString(), mMessage);


        long rowId = mAlarmsDBWrapper.addAlarm(alarm);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        AlarmNotificationHandler.updateNotification(notificationManager, mContext, mAlarmsDBWrapper.getAlarms());


//        Log.d(TAG, "DB Row Id: " + rowId);
//        Log.d(TAG, "Location: " + mLocationName);
//        Log.d(TAG, "Latitude: " + mLatitude);
//        Log.d(TAG, "Longitude: " + mLongitude);
//        Log.d(TAG, "Distance: " + mDistance);
//        Log.d(TAG, "Label: " + mLabel);
//        Log.d(TAG, "Vibrate: " + mVibrate);
//        Log.d(TAG, "Whatsapp: " + mWhatsApp);
//        Log.d(TAG, "People: " + mPeople.toString());
//        Log.d(TAG, "Numbers: " + mNumbers.toString());
//        Log.d(TAG, "Message: " + mMessage);

        String toastMessage = getString(R.string.add_alarm_confimartion_toast_message);

        Toast.makeText(this, alarm.getLabel() + toastMessage, Toast.LENGTH_SHORT).show();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("latitude", mLatitude);
        returnIntent.putExtra("longitude", mLongitude);
        returnIntent.putExtra("radius", mDistance);
        returnIntent.putExtra("label", mLabel);
        setResult(RESULT_OK, returnIntent);
        finish();

        return true;
    }

}

package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBHelper;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;

/**
 * Created by leonardogcsoares on 7/23/2015.
 */
public class BootStartAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent intentLaunch = new Intent(context, MapsActivity.class);
        PendingIntent notifPendingIntent = PendingIntent.getActivity(context, 0, intentLaunch, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmsDBHelper alarmsDBHelper = new AlarmsDBHelper(context);
        AlarmsDBWrapper alarmsDBWrapper = new AlarmsDBWrapper(alarmsDBHelper);


        // Checks if any alarms on before launching notification
        List<Alarm> alarms = alarmsDBWrapper.getAlarms();
        boolean alarmsOn = false;
        for (int i=0; i<alarms.size(); i++) {
            if (alarms.get(i).isOn()) {
                alarmsOn = true;
                break;
            }
        }
        if (alarmsOn) {
            Notification notification =
                    new NotificationCompat.Builder(context)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.paper_airplane_notif))
                            .setSmallIcon(R.drawable.paper_airplane_notif)
                            .setContentTitle(context.getString(R.string.app_name))
                            .setContentText(context.getString(R.string.on_boot_broadcast_receiver))
                            .setColor(context.getResources().getColor(android.R.color.holo_blue_light))
                            .setLights(android.R.color.holo_blue_bright, 1000, 1000)
                            .setContentIntent(notifPendingIntent).build();
//                                .addAction(R.drawable.ic_action_close, getString(R.string.notification_close_button), notifPendingIntent).build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(1, notification);
        }
    }
}

package inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase;

import android.provider.BaseColumns;

/**
 * Created by leonardogcsoares on 3/27/2015.
 */
// Static variables that denotate the column names in the table.
public abstract class AlarmEntries implements BaseColumns {
    public static final String TABLE_NAME = "Alarms";
    public static final String COLUMN_ALARM_ID = "idAlarm";
    public static final String COLUMN_ALARM_LATITUDE = "latitudeAlarm";
    public static final String COLUMN_ALARM_LONGITUDE = "longitudeAlarm";
    public static final String COLUMN_ALARM_LOCATION = "locationAlarm";
    public static final String COLUMN_ALARM_DISTANCE = "radiusAlarm";
    public static final String COLUMN_ALARM_LABEL = "labelAlarm";
    public static final String COLUMN_ALARM_VIBRATE = "vibrateAlarm";
    public static final String COLUMN_ALARM_WHATSAPP = "whatsappAlarm";
    public static final String COLUMN_ALARM_PEOPLE = "peopleAlarm";
    public static final String COLUMN_ALARM_NUMBERS = "numbersAlarm";
    public static final String COLUMN_ALARM_CUSTOM_MESSAGE = "customMessageAlarm";
    public static final String COLUMN_ALARM_IS_ON = "isOnAlarm";
    public static final String COLUMN_NAME_NULLABLE= "null";
}

package inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by leonardogcsoares on 3/14/2015.
 */
public class AlarmsDBHelper extends SQLiteOpenHelper {

    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String BOOLEAN_TYPE = " BOOLEAN";
    private static final String UNSIGNED_TINYINT_TYPE = " TINYINT UNSIGNED";
    private static final String VARCHAR_TYPE = " VARCHAR";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + AlarmEntries.TABLE_NAME + " (" +
                    AlarmEntries.COLUMN_ALARM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_LATITUDE + " INTEGER" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_LONGITUDE + " INTEGER" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_LOCATION + VARCHAR_TYPE + "(255) NOT NULL" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_DISTANCE + INT_TYPE + " NOT NULL" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_LABEL + VARCHAR_TYPE + "(50)" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_VIBRATE + BOOLEAN_TYPE + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_WHATSAPP + BOOLEAN_TYPE + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_PEOPLE + VARCHAR_TYPE + "(255)" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_NUMBERS + VARCHAR_TYPE + "(255)" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_CUSTOM_MESSAGE + VARCHAR_TYPE + "(255)" + COMMA_SEP +
                    AlarmEntries.COLUMN_ALARM_IS_ON + BOOLEAN_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + AlarmEntries.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Alarms.db";

    public AlarmsDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDownGrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}

package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.TransitionDrawable;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBHelper;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;

/**
 * Created by leonardogcsoares on 6/20/2015.
 */
public class WakeAlarmActivity extends Activity {

    private static final String TAG = "TestAlarmActivity";
    FloatingActionButton fab;
    FrameLayout frameLayoutReg1;
    FrameLayout frameLayoutReg5;
    RelativeLayout relativeLayoutBackground;
    ViewSwitcher textViewSwitcher;
    RelativeLayout textView1ViewSwitcher;
    RelativeLayout textView2ViewSwitcher;
    TextView alarmNameTextView;

    List<Alarm> alarmsList;
    Alarm playingAlarm;
    private Vibrator vibrator;
    Ringtone defaultRingtone;

    private int currentRingerMode;
    private AudioManager audioManager;

    private static final int notifCode = 0;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_alarm_activity);

        mContext = this;

        AdView mAdView = (AdView) findViewById(R.id.wakeupAlarm_activity_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Bundle receiveBundle = getIntent().getExtras();
        final String triggeredGeofence;
        if(receiveBundle != null) {
            triggeredGeofence = receiveBundle.getString("REQUEST_ID");
        }
        else {
            triggeredGeofence = "NOT INITIALIZED";
        }

        alarmNameTextView = (TextView) findViewById(R.id.textView_1_viewSwitcher_text);
        alarmNameTextView.setText(triggeredGeofence);


        Intent intent = new Intent(mContext, WakeAlarmActivity.class);
        PendingIntent notifPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification=
                new NotificationCompat.Builder(this)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.paper_airplane_notif))
                        .setSmallIcon(R.drawable.paper_airplane_notif)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(triggeredGeofence)
                        .setOngoing(true)
                        .setColor(getResources().getColor(android.R.color.holo_blue_light))
                        .setLights(android.R.color.holo_blue_bright, 1000, 1000)
                        .setContentIntent(notifPendingIntent).build();
//                                .addAction(R.drawable.ic_action_close, getString(R.string.notification_close_button), notifPendingIntent).build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifCode, notification);



        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                AlarmsDBHelper alarmsDBHelper = new AlarmsDBHelper(mContext);
                AlarmsDBWrapper alarmsDBWrapper = new AlarmsDBWrapper(alarmsDBHelper);
                alarmsList = alarmsDBWrapper.getAlarms();


                Log.d(TAG, "Triggered: " + triggeredGeofence);
                for (int i=0; i<alarmsList.size(); i++) {
                    if (alarmsList.get(i).getLabel().equals(triggeredGeofence)){
                        playingAlarm = alarmsList.get(i);
                        playingAlarm.setIsOn(false); // Turn alarm off
                        alarmsDBWrapper.updateAlarmOnOff(playingAlarm);
                        Log.d(TAG, "Alarm " + triggeredGeofence + " disabled!");

                        if (playingAlarm.isVibrate()) {
                            long[] pattern = {0, 700, 700};
                            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(pattern, 0);
                        }
                    }
                }

                return null;
            }
        }.execute();

        relativeLayoutBackground = (RelativeLayout) findViewById(R.id.test_relativeLayout);
        frameLayoutReg1 = (FrameLayout) findViewById(R.id.test_activity_region1);
        frameLayoutReg5 = (FrameLayout) findViewById(R.id.test_activity_region5);

        textViewSwitcher = (ViewSwitcher) findViewById(R.id.text_viewSwitcher);
        textView1ViewSwitcher = (RelativeLayout) findViewById(R.id.textView_1_viewSwitcher);
        textView2ViewSwitcher = (RelativeLayout) findViewById(R.id.textView_2_viewSwitcher);


        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        currentRingerMode = audioManager.getRingerMode();
        audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        audioManager.setStreamVolume(AudioManager.STREAM_RING, audioManager.getStreamMaxVolume(AudioManager.STREAM_RING), 0);

        defaultRingtone = RingtoneManager.getRingtone(this, Settings.System.DEFAULT_RINGTONE_URI);
        defaultRingtone.play();

        fab = (FloatingActionButton) findViewById(R.id.test_floating_button);
        final Animation shakeAnim = AnimationUtils.loadAnimation(WakeAlarmActivity.this, R.anim.shake_fab_wakealarm);
        shakeAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fab.startAnimation(shakeAnim);
                    }
                }, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fab.startAnimation(shakeAnim);


        fab.setOnTouchListener(new View.OnTouchListener() {
                                   @Override
                                   public boolean onTouch(View v, MotionEvent event) {
                                       switch (event.getAction()) {
                                           case MotionEvent.ACTION_DOWN:
                                               ClipData data = ClipData.newPlainText("", "");
                                               View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(null);
                                               fab.startDrag(data, shadowBuilder, null, 0);
                                               fab.clearAnimation();
                                               break;

                                           case MotionEvent.ACTION_MOVE:
                                               break;

                                           case MotionEvent.ACTION_UP:
                                               break;

                                       }
                                       return true;
                                   }
                               }
        );

        TransitionDrawable transition = (TransitionDrawable) relativeLayoutBackground.getBackground();
        OuterDragEventListener outerDragEventListener = new OuterDragEventListener(transition, notification);
        frameLayoutReg1.setOnDragListener(outerDragEventListener);
        frameLayoutReg5.setOnDragListener(outerDragEventListener);

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "Only one intent is created...");
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    protected class OuterDragEventListener implements View.OnDragListener {

        TransitionDrawable mTransition;
        Notification mNotification;

        public  OuterDragEventListener(TransitionDrawable transition, Notification notification) {
            this.mTransition = transition;
            this.mNotification = notification;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch(event.getAction()) {

                case DragEvent.ACTION_DRAG_STARTED:
                    fab.setAlpha(0f);
                    return true;

                case DragEvent.ACTION_DRAG_EXITED:
                    mTransition.resetTransition();
                    textViewSwitcher.showPrevious();
                    Log.d(TAG, "Drag exited");
                    return true;

                case DragEvent.ACTION_DRAG_ENTERED:
                    mTransition.startTransition(150);
                    textViewSwitcher.showNext();
                    return true;

                case DragEvent.ACTION_DROP:
                    fab.clearAnimation();
                    fab.setVisibility(View.GONE);
                    defaultRingtone.stop();
                    audioManager.setRingerMode(currentRingerMode);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(notifCode);
                    AlarmNotificationHandler.updateNotification(notificationManager, mContext, alarmsList);
                    if (playingAlarm.isVibrate())
                        vibrator.cancel();
                    synchronized (this) {
                        try {
                            wait(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Intent intent = new Intent(mContext, MapsActivity.class);
                    startActivity(intent);
                    return true;

                case DragEvent.ACTION_DRAG_ENDED:
                    fab.setAlpha(1f);
                    return true;
            }

            return false;
        }
    }
}

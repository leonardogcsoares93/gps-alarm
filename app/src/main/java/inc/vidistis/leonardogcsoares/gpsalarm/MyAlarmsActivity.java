package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;
import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.AlarmsDBWrapper;

/**
 * Created by leonardogcsoares on 4/3/2015.
 */
public class MyAlarmsActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    public static final int alarmOnNotifCode = 2;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    private List<Alarm> mAlarmList;
    private static final String TAG = "MyAlarmsActivity";
    private Activity mActivity;
    AlarmsDBWrapper mAlarmsDBWrapper;
//    protected static List<Geofence> mGeofenceList = new ArrayList<Geofence>();

    private GoogleApiClient mGoogleApiClient;

    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        mAlarmsDBWrapper = new AlarmsDBWrapper();
        mAlarmList = mAlarmsDBWrapper.getAlarms();

        mActivity = this;
        mContext = this;

        if (mAlarmList.isEmpty()){
            setContentView(R.layout.activity_my_alarms_empty);
            ImageView emptyImageView = (ImageView) findViewById(R.id.alarms_empty_imageView);
            emptyImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        else {
            setContentView(R.layout.activity_my_alarms);
            mRecyclerView = (RecyclerView) findViewById(R.id.my_alarms_activity_recyclerView);

            // Improves performance by telling the recycler view that changes in content won't affect the layout size of the view
//        mRecyclerView.setHasFixedSize(true);

            mRecyclerViewLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mRecyclerViewLayoutManager);

            //specifying the adapter

            AlarmAdapter alarmAdapter = new AlarmAdapter();
            mRecyclerView.setAdapter(alarmAdapter);
        }

        AdView mAdView = (AdView) findViewById(R.id.my_alarms_activity_adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


    }

    @Override
    protected void onStart() {
        super.onStart();
        buildGoogleApiClient();
    }

    @Override
    protected void onStop() {
        if(mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        super.onStop();
    }



    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
//        new GeofencingHandler.AddGeofencesAsync(mContext, mGoogleApiClient, mAlarmsDBWrapper.getAlarms()).execute();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder> {


        public class AlarmViewHolder extends RecyclerView.ViewHolder {
            protected TextView mLabel;
            protected TextView mLocation;
            protected TextView mDistance;
            protected ToggleButton mMonday;
            protected ToggleButton mTuesday;
            protected ToggleButton mWednesday;
            protected ToggleButton mThursday;
            protected ToggleButton mFriday;
            protected ToggleButton mSaturday;
            protected ToggleButton mSunday;
            protected ImageView mTrashImageButton;
            protected Switch mOnSwitch;

            public AlarmViewHolder(View view) {
                super(view);

                this.mLabel = (TextView) view.findViewById(R.id.my_alarms_label_textView);
                this.mLocation = (TextView) view.findViewById(R.id.my_alarms_location_textView);
                this.mDistance = (TextView) view.findViewById(R.id.my_alarms_distance_textView);
//                this.mMonday = (ToggleButton) view.findViewById(R.id.my_alarms_monday_toggleButton);
//                this.mTuesday = (ToggleButton) view.findViewById(R.id.my_alarms_tuesday_toggleButton);
//                this.mWednesday = (ToggleButton) view.findViewById(R.id.my_alarms_wednesday_toggleButton);
//                this.mThursday = (ToggleButton) view.findViewById(R.id.my_alarms_thursday_toggleButton);
//                this.mFriday = (ToggleButton) view.findViewById(R.id.my_alarms_friday_toggleButton);
//                this.mSaturday = (ToggleButton) view.findViewById(R.id.my_alarms_saturday_toggleButton);
//                this.mSunday = (ToggleButton) view.findViewById(R.id.my_alarms_sunday_toggleButton);
                this.mTrashImageButton = (ImageView) view.findViewById(R.id.my_alarms_trash_imageButton);
                this.mOnSwitch = (Switch) view.findViewById(R.id.my_alarms_turn_on_switch);

            }

        }

        @Override
        public AlarmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_alarms_row, parent, false);

             return new AlarmViewHolder(v);
        }


        @Override
        public void onBindViewHolder(final AlarmViewHolder holder, final int position) {
            holder.mLabel.setText(mAlarmList.get(position).getLabel());
            holder.mLabel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("latitude", mAlarmList.get(position).getLatitude());
                    returnIntent.putExtra("longitude", mAlarmList.get(position).getLongitude());
                    returnIntent.putExtra("radius", mAlarmList.get(position).getDistance());
                    returnIntent.putExtra("label", mAlarmList.get(position).getLabel());
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
            holder.mLocation.setText(mAlarmList.get(position).getLocation());
            holder.mDistance.setText("R:   " + Integer.toString(mAlarmList.get(position).getDistance()) + " m");
            holder.mOnSwitch.setChecked(mAlarmList.get(position).isOn());
            Log.d(TAG, mAlarmList.get(position).getLabel() + " is on: " + Boolean.toString(mAlarmList.get(position).isOn()));

            holder.mTrashImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Log.d(TAG, "Database rows before: " + Integer.toString(mAlarmsDBWrapper.getAlarmsCount()));
                    Log.d(TAG, "position" + Integer.toString(position) + " size: " + mAlarmList.size());
//                    Log.d(TAG, "Alarm id: " + Integer.toString(mAlarmList.get(position).getId()));
                    try {
                        mAlarmsDBWrapper.removeAlarm(mAlarmList.get(position), mAlarmList.size());
                        notifyItemRemoved(position);
                        mAlarmList.remove(position);
                    }
                    catch (IndexOutOfBoundsException e) {
                        // Still needs to be fixed
                        notifyItemRemoved(0);
                        mAlarmList.remove(0);
                    }
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    AlarmNotificationHandler.updateNotification(notificationManager, mContext, mAlarmList);
//                    Log.d(TAG, "Database rows after: " + Integer.toString(mAlarmsDBWrapper.getAlarmsCount()));
                }
            });


            final String text = holder.mLabel.getText().toString();
            holder.mOnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    if (isChecked) {
                        Toast.makeText(mContext, text + " was turned on", Toast.LENGTH_SHORT).show();
                        // Update database with addition
                        mAlarmList.get(position).setIsOn(true);
                        mAlarmsDBWrapper.updateAlarmOnOff(mAlarmList.get(position));
                        AlarmNotificationHandler.updateNotification(notificationManager, mContext, mAlarmList);

                        // Create snackbar with action to go to maps...
                    }

                    else {
                        //Update database with removal
                        mAlarmList.get(position).setIsOn(false);
                        mAlarmsDBWrapper.updateAlarmOnOff(mAlarmList.get(position));
                        AlarmNotificationHandler.updateNotification(notificationManager, mContext, mAlarmList);

                    }
                }
            });

        }
        @Override
        public int getItemCount() {
            return mAlarmList.size();
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

    }


}

package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase.Alarm;

/**
 * Created by leonardogcsoares on 7/3/2015.
 */
public class GeofencingHandler {

    private static List<Geofence> mGeofences;

    private static final String TAG = "GeofencingHandler";

    private GeofencingHandler() {
    }

    public static class AddGeofencesAsync extends AsyncTask<Void, Void, Void> {

        Context mContext;
        GoogleApiClient mGoogleApiClient;
        List<Alarm> mAlarms;
        List<Geofence> mGeofenceList;
        boolean mAddSuccess;

        public AddGeofencesAsync(Context context, GoogleApiClient googleApiClient, List<Alarm> alarms) {
            this.mContext = context;
            this.mGoogleApiClient = googleApiClient;
            this.mAlarms = alarms;
            this.mAddSuccess = false;
        }

        @Override
        protected Void doInBackground(Void... params) {

            mGeofenceList = GeofencingHandler.createGeofenceList(mAlarms);
            mGeofences = mGeofenceList;

            mAddSuccess = false;
            // When the activity is started by a startActivityForResult() returns different than null
            Log.d(TAG, "mGoogleApiClient is connected: " + mGoogleApiClient.toString());
            if (!mGeofenceList.isEmpty()) {
                GeofencingHandler.removeGeofences(mContext, mGoogleApiClient);
                GeofencingHandler.addGeofences(mContext, mGoogleApiClient, mGeofenceList);
                mAddSuccess = true;
//                Log.d(TAG, "Geofences added successfully: " + Integer.toString(mGeofenceList.size()));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (!mGeofenceList.isEmpty() && mAddSuccess) {
                Toast.makeText(mContext, mContext.getString(R.string.geofence_handler_add_success), Toast.LENGTH_SHORT).show();
                Log.d(TAG, mGeofenceList.toString());
            }
        }
    }

    public synchronized static List<Geofence> createGeofenceList(List<Alarm> alarms) {
        List<Geofence> geofenceList = new ArrayList<>();
        if (!alarms.isEmpty()) {
            for (int i=0; i<alarms.size(); i++) {
                if (alarms.get(i).isOn()) {
                    geofenceList.add(new Geofence.Builder()
                            .setRequestId(alarms.get(i).getLabel())
                            .setCircularRegion(
                                    alarms.get(i).getLatitude(),
                                    alarms.get(i).getLongitude(),
                                    (float) alarms.get(i).getDistance()
                            )
                            .setExpirationDuration(Geofence.NEVER_EXPIRE)
                            .setTransitionTypes(GeofencingRequest.INITIAL_TRIGGER_ENTER | Geofence.GEOFENCE_TRANSITION_ENTER)
                            .build());
                }
            }
        }
//        Log.d(TAG, geofenceList.toString());
        return geofenceList;
    }

    // Geofencing Methods (Adding, Removing, Monitiring)
    public synchronized static GeofencingRequest getGeofencingRequest(List<Geofence> geofenceList) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofenceList);
        return builder.build();
    }

    public synchronized static PendingIntent getGeofencePendingIntent(Context context) {

        Intent intent = new Intent(context, GeofenceTransitionsIntentService.class);
        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public synchronized static void addGeofences(Context context, GoogleApiClient googleApiClient, List<Geofence> geofenceList) {
        LocationServices.GeofencingApi.addGeofences(
                googleApiClient,
                getGeofencingRequest(geofenceList),
                getGeofencePendingIntent(context)
        ).setResultCallback(getResultCallback());

        try {
            Log.d(TAG, "addGeofences() Geofences added: " + getResultCallback().toString());
        }
        catch (NullPointerException e){
            Log.d(TAG, "addGeofences() Geofences not added: ");
        }
    }

    public synchronized static void removeGeofences(Context context, GoogleApiClient googleApiClient) {
//        LocationServices.GeofencingApi.removeGeofences(
//                googleApiClient,
//                getGeofencePendingIntent(context)
//        ).setResultCallback(getResultCallback());


        List<String> geofenceLabels = new ArrayList<>();
        for (int i=0; i<mGeofences.size(); i++){
            geofenceLabels.add(mGeofences.get(i).getRequestId());
        }
        LocationServices.GeofencingApi.removeGeofences(googleApiClient,
                geofenceLabels
                ).setResultCallback(getResultCallback());


    }

    public synchronized static ResultCallback getResultCallback() {
        return new ResultCallback() {
            @Override
            public void onResult(Result result) {
                Log.d(TAG, "Result callback: " + result.toString());
            }
        };
    }
}

package inc.vidistis.leonardogcsoares.gpsalarm;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;


/**
 * Created by leonardogcsoares on 7/17/2015.
 */
public class PlanATripActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_alarms_empty);

        TextView textView = (TextView) findViewById(R.id.my_alarms_empty_text);
        textView.setText(getString(R.string.plan_a_trip_placeholder_text));
    }
}

package inc.vidistis.leonardogcsoares.gpsalarm.alarmDatabase;

import java.util.ArrayList;

/**
 * Created by leonardogcsoares on 3/24/2015.
 */
public class Alarm {

    private int mId;
    private double mLatitude;
    private double mLongitude;
    private String mLocation;
    private int mDistance;
    private String mLabel;
    private boolean mVibrate;
    private boolean mWhatsapp;
    private String mPeople;
    private String mNumbers;
    private String mMessage;
    private boolean isOn;
    private boolean[] mDays = new boolean[7];


    public static final int MONDAY = 0;
    public static final int TUESDAY = 1;
    public static final int WEDNESDAY = 2;
    public static final int THURSDAY = 3;
    public static final int FRIDAY = 4;
    public static final int SATURDAY = 5;
    public static final int SUNDAY = 6;

    public Alarm(int mId, double mLatitude, double mLongitude, String mLocation,
                 int mDistance, String mLabel, boolean mVibrate, boolean mWhatsapp,
                 String mPeople, String mNumbers, String mMessage, boolean isOn) {
        this.mId = mId;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mLocation = mLocation;
        this.mDistance = mDistance;
        this.mLabel = mLabel;
        this.mVibrate = mVibrate;
        this.mWhatsapp = mWhatsapp;
        this.mPeople = mPeople;
        this.mNumbers = mNumbers;
        this.mMessage = mMessage;
        this.isOn = isOn;
    }

    public Alarm(double mLatitude, double mLongitude, String mLocation,
                 int mDistance, String mLabel, boolean mVibrate, boolean mWhatsapp,
                 String mPeople, String mNumbers, String mMessage) {
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.mLocation = mLocation;
        this.mDistance = mDistance;
        this.mLabel = mLabel;
        this.mVibrate = mVibrate;
        this.mWhatsapp = mWhatsapp;
        this.mPeople = mPeople;
        this.mNumbers = mNumbers;
        this.mMessage = mMessage;
        this.isOn = false;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String mLocation) {
        this.mLocation = mLocation;
    }

    public int getDistance() {
        return mDistance;
    }

    public void setDistance(int mDistance) {
        this.mDistance = mDistance;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String mLabel) {
        this.mLabel = mLabel;
    }

    public boolean isVibrate() {
        return mVibrate;
    }

    public void setVibrate(boolean mVibrate) {
        this.mVibrate = mVibrate;
    }

    public boolean isWhatsapp() {
        return mWhatsapp;
    }

    public void setWhatsapp(boolean mWhatsapp) {
        this.mWhatsapp = mWhatsapp;
    }

    public String getPeople() {
        return mPeople;
    }

    public void setPeople(String mPeople) {
        this.mPeople = mPeople;
    }

    public String getNumbers() {
        return mNumbers;
    }

    public void setNumbers(String mNumbers) {
        this.mNumbers = mNumbers;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String toString() {
        String str = "\nId: " + Integer.toString(this.getId()) +
                "\nLatitude: " + Double.toString(getLatitude()) +
                "\nLongitude: " + Double.toString(this.getLongitude()) +
                "\nLocation: " + getLocation() +
                "\nDistance: " + Integer.toString(getDistance()) +
                "\nLabel: " + getLabel() +
                "\nisVibrate: " + isVibrate() +
                "\nisWhatsapp: " + isWhatsapp() +
                "\nPeople: " + getPeople() +
                "\nNumbers: " + getNumbers() +
                "\nMessage: " + getMessage() +
                "\nIsOn: " + Boolean.toString(isOn);

        return str;
    }

    public void setIsOn(boolean alarmOn){
        this.isOn = alarmOn;
    }

    public boolean isOn(){
        return isOn;
    }


}
